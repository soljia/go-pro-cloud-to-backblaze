from pathlib import Path
import os
import schedule
import time
import logging
import json
from datetime import datetime, timedelta
from b2sdk.v2 import InMemoryAccountInfo
from b2sdk.v2 import *
import requests
from tqdm import tqdm
import hashlib
import sqlite3
from urllib.parse import urlparse
from operator import itemgetter

def initLog():
    level,_format,path,name = itemgetter("level","_format","path","name")(config["log"])
    logging.basicConfig(
        level=level,
        format=_format,
        handlers=[
            logging.FileHandler("{0}/{1}".format(path,name)),
            logging.StreamHandler()
        ])

def gopro_getToken():
    finalHeader = dict()
    finalHeader.update(config["gopro"]["http"]["login"]["headers"])
    r = requests.post(config["gopro"]["http"]["login"]["url"], headers=finalHeader, data=config["gopro"]["http"]["login"]["payload"])
    return r.json()["access_token"]

def run():
    b2auth() #Authenticate to BackBlaze B2
    bucket = b2_api.get_bucket_by_name(config["backblaze"]["bucket"]) # Get bucket
    gpids = bb_getGPIDs(bucket) # Get existing GoPro media in Backblakze (GPIDs)
    
    latestMedia = gopro_listmedia() #Get GoPro media list
    if "error_description" in latestMedia.keys():
        return logger.debug(latestMedia["error_description"])

    latestMedia = sorted(latestMedia["_embedded"]["media"], key=itemgetter('file_size')) #Sort by size
    
    for media in latestMedia: #Iterate over all go pro media fetched
        filename, id = itemgetter('filename', 'id')(media)
        if id not in gpids:  #If media id (gpid) not in BackBlaze gpid list then upload
            logger.debug(f"Uploading {filename} : {id}")
            transferToB2(bucket, filename, id)
        else:
            logger.debug(f"{filename} : {id} already exists!")

def downloadURL(id):
    return f"https://api.gopro.com/media/{id}/download"

def gopro_listmedia():
    listToken = gopro_getToken()
    logger.debug(f"List Token: {listToken}")
    finalHeader = dict()
    finalHeader.update(config["gopro"]["http"]["list"]["headers"])
    finalHeader.update({'Authorization':f"Bearer {listToken}"})
    
    page, count, fromDate, toDate, pastX, dateFormat = itemgetter("page", "count", "fromDate","toDate", "pastX", "dateFormat")(config["gopro"]["searchOptions"]) #Get config options
    
    fromDate = fromDate if fromDate else (today - timedelta(**pastX)).strftime(dateFormat)
    toDate = toDate if toDate else (today + timedelta(1)).strftime(dateFormat)
    params = {'fields':'filename,file_size,id', 'processing_states':'ready','order_by':'created_at','per_page':count,'page':page,'created_range':f"{fromDate},{toDate}"}

    url = "https://api.gopro.com/media/search"
    r = requests.get(url, headers=finalHeader, params= params)
    
    json = r.json()
    return json

def b2auth():
    b2_api.authorize_account("production", config["backblaze"]["api"]["application_key_id"], config["backblaze"]["api"]["application_key"])
    
def bb_getGPIDs(bucket):
    bucketList = bucket.ls(latest_only=True)
    arr = list()
    logger.debug(f"Retrieving GPIDs from BackBlaze")
    for file_version, folder_name in bucketList:
        file_info = file_version.as_dict()['fileInfo']
        #logger.debug(f"File Info: {file_info}")
        if "gpid" in file_info.keys():
            arr.append(file_info["gpid"])
    return arr

def transferToB2(bucket, filename, id):
    #Get media file information
    finalHeader = dict()
    finalHeader.update(config["gopro"]["http"]["download"]["headers"])
    finalHeader.update({'Authorization':config["gopro"]["http"]["download"]["token"]})
    r = requests.get(downloadURL(id),headers=finalHeader)
    data = r.json()
    
    #Get original quality
    url = data["_embedded"]["variations"][0]["url"]
    r = requests.get(url, stream=True)
 
    #Download file to local storage
    with open(filename, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=128):
            fd.write(chunk)
        fd.close()
        
    
    #Upload file to Backblaze
    listen = TqdmProgressListener(description=f"Uploading file {filename}")
    file_info = {'gpid':id}
    with open(filename, 'r') as fd:
        bucket.upload_local_file(
        local_file=filename,
        file_name=filename,
        progress_listener=listen,
        file_infos=file_info)
    
    logger.info(f"Uploaded {filename} to BackBlaze. Removing file.")
    
    #Remove temp downloaded file
    if(bool(config["general"]["deleteLocal"])):
        try:
            os.remove(filename)
            logger.debug(f"{filename} deleted.")
        except:
            logger.error(f"Error while deleting {filename}")

#Get config
with open('config.json', 'r') as f:
    config = json.load(f)

today = datetime.now()
initLog()

logger = logging.getLogger()
logger.debug("Today: {0}".format(today))
logger.debug(config)
info = InMemoryAccountInfo()  # store credentials, tokens and cache in memory
b2_api = B2Api(info)
  
run()

schedule.every(24).hours.do(run)

while 1:
    schedule.run_pending()
    time.sleep(1)
